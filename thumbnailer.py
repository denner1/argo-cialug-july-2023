#!/usr/bin/env python3
import argparse
import json
import numpy as np
from glob import glob
from PIL import Image
import os


def get_thumb_path(thumb_dir, file_name):
    return os.path.join(thumb_dir, os.path.basename(file_name))


def resize(thumb_dir, source_img):
    image = Image.open(source_img)
    image.thumbnail((500,500), Image.LANCZOS)
    thumb_path = get_thumb_path(thumb_dir, source_img)
    image.save(thumb_path)

def resize_list(thumb_dir, images):
    for img in images:
        resize(thumb_dir, img)

def map():
    pass

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--map', action='store_true', required=False)
    parser.add_argument('images', nargs='*')
    args = parser.parse_args()
    thumb_dir = '/data/thumbs'
    if args.map:
        try:
            os.makedirs(thumb_dir)
        except:
            pass
        images = glob('/data/*.jpg') + glob('/data/*.jpeg') + glob('/data/*.png')
        sublists = np.array_split(images, 10)
        sublists = [x.tolist() for x in sublists]
        sublist_json = json.dumps(sublists)
        print(sublist_json)
    elif args.images:
        if len(args.images) == 1:
            image_list = json.loads(args.images[0])
        resize_list(thumb_dir, image_list)

if __name__ == "__main__":
    main()
